package com.example.sfacg_crawler.data

import androidx.lifecycle.*
import kotlinx.coroutines.launch

class RecordViewModel(private val repository: RecordRepository): ViewModel() {
    val allRecord: LiveData<List<Record>> = repository.getAllRecord.asLiveData()

    fun insertRecord(record: Record) = viewModelScope.launch {
        repository.insert(record)
    }

    fun deleteRecord(record: Record) = viewModelScope.launch {
        repository.delete(record)
    }

    fun updateRecord(record: Record) = viewModelScope.launch {
        repository.update(record)
    }

    // insert-1, delete-2, update-3
    var whichOperation = 0



}

class RecordViewModelFactory(private val repository: RecordRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(RecordViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return RecordViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}
package com.example.sfacg_crawler.data

import android.util.Log
import kotlinx.coroutines.flow.Flow

class RecordRepository(private val recordDao: RecordDao) {
    // private val crawler = Crawler()
    val getAllRecord: Flow<List<Record>> = recordDao.getAllRecord()

    suspend fun insert(record: Record) {
        recordDao.insertRecord(record)
    }

    suspend fun delete(record: Record) {
        recordDao.deleteRecord(record)
    }

    suspend fun update(record: Record) {
        recordDao.updateRecord(record)
    }
}
package com.example.sfacg_crawler.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "record")
class Record(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,

    @ColumnInfo(name = "time")
    var time: String = "",

    @ColumnInfo(name = "name")
    var recordName: String = "",

    @ColumnInfo(name = "stag")
    var stag: Int = 145,

    @ColumnInfo(name = "start_page")
    var start: Int = 1,

    @ColumnInfo(name = "end_page")
    var end: Int = 0,

    @ColumnInfo(name = "now_page")
    var nowPage: Int = 1,

    @ColumnInfo(name = "status")
    var status: Boolean = false
) {
    constructor(
        time: String,
        recordName: String,
        stag: Int,
        start: Int,
        end: Int,
        nowPage: Int,
        status: Boolean,
    ): this() {
        this.time = time
        this.recordName = recordName
        this.stag = stag
        this.start = start
        this.end = end
        this.nowPage = nowPage
        this.status = status
    }
    constructor(
        id: Int
    ): this() {
        this.id = id
    }
    constructor(
        id: Int,
        nowPage: Int,
    ): this(){
        this.id = id
        this.nowPage = nowPage
    }
}


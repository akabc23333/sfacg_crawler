package com.example.sfacg_crawler.data

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class RecordApplication: Application() {
    private val applicationScope = CoroutineScope(SupervisorJob())
    private val database by lazy { RecordDatabase.getDatabase(this, applicationScope) }
    val repository by lazy { RecordRepository(database.recordDao()) }
}
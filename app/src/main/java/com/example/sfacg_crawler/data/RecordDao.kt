package com.example.sfacg_crawler.data


import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface RecordDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecord(record: Record)

    @Delete
    suspend fun deleteRecord(record: Record)

    @Query("SELECT * FROM record")
    fun getAllRecord(): Flow<List<Record>>

    @Update
    suspend fun updateRecord(record: Record)


}


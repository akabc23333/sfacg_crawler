package com.example.sfacg_crawler.data

import android.util.Log
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class Crawler {
    private val url = "https://book.sfacg.com"
    private lateinit var doc: Document
    lateinit var result: BookList

    fun request(tag: Int, page: Int): Boolean {
        try {
            doc = Jsoup.parse(Jsoup.connect("$url/stag/$tag/$page/").get().html())
        }catch (e: Exception){
            return false
        }
        val bookLink = getBookLink()
        Log.d("d", "crawler get doc")
        result = BookList(
            getBookName(),
            getBookCover(),
            bookLink,
            getBookType(),
            getBookAuthor(bookLink),
            getBookStatus(),
            getBookDate(),
            getBookWord(),
            getBookSynopsis()
        )
        return true
    }

    // 获取小说名称
    private fun getBookName(): List<String> {
        return doc.selectXpath("//ul[@class=\"TAG_list cover clearer2\"]/li/strong/a").eachText()
    }

    // 获取小说封面链接
    private fun getBookCover(): List<String> {
        val bookCovers = mutableListOf<String>()
        doc.selectXpath("//ul[@class=\"TAG_list cover clearer2\"]/li/a/img").forEach {
            val bookCover = it.attr("src")
            bookCovers.add(bookCover)
        }
        return bookCovers
    }

    // 获取小说链接
    private fun getBookLink(): List<String> {
        val bookLinks = mutableListOf<String>()
        doc.selectXpath("//ul[@class=\"TAG_list cover clearer2\"]/li/strong/a").forEach {
            val bookLink = it.attr("href")
            bookLinks.add(bookLink)
        }
        return bookLinks
    }

    // 获取小说类型
    private fun getBookType(): List<String> {
        return doc.selectXpath("//ul[@class=\"TAG_list cover clearer2\"]/li/a").eachText()
    }

    // 获取小说简要信息
    private fun getBookInformation(): List<List<String>> {
        val bookInformations = mutableListOf<List<String>>()
        doc.selectXpath("//ul[@class=\"TAG_list cover clearer2\"]/li").eachText().forEach {
            val bookInformation = it.split(" / ")
            bookInformations.add(bookInformation)
        }
        return bookInformations
    }
    /*
    百物语之主 综合信息：9.1(1119人评分),
    66338人收藏,
    2412.5万人看过,
    魔幻,
    已完结,
    2021/5/23,
    5062310字 　　　　穿越到玄幻版的战国东瀛，百鬼夜行，战乱不断。 　　本以为我也能进入玄幻的领域，却没想到自己没有任何修炼天赋，武士、忍者、僧人、阴阳师一个也当不了。 　　好不容易穿越一次，人生没追求和咸鱼还有什么区别？ 　　哪怕是堕入妖道，甚至变成女孩又怎么样？我依然要逆天修行，我必为妖帝！ 　　胆敢奴役我，把我当做式神的这个混蛋，我必把他… 　　正宗:小灯你要把我怎么样啊？ 　　灯莹:哇！主人！什么都没有说！..
    * */

    // 获取小说收藏数
    private fun getBookCollection(): List<String> {
        val bookCollections = mutableListOf<String>()
        getBookInformation().forEach {
            bookCollections.add(Regex("""\d+""").find(it[1])?.value.toString())
        }
        return bookCollections
    }

    // 获取小说状态
    private fun getBookStatus(): List<String> {
        val bookStatus = mutableListOf<String>()
        getBookInformation().forEach {
            bookStatus.add(it[4])
        }
        return bookStatus
    }

    // 获取小说最后更新日期
    private fun getBookDate(): List<String> {
        val bookDate = mutableListOf<String>()
        getBookInformation().forEach {
            bookDate.add(it[5])
        }
        return bookDate
    }

    // 获取小说字数
    private fun getBookWord(): List<String> {
        val bookWord = mutableListOf<String>()
        getBookInformation().forEach {
            bookWord.add(Regex("""\d+""").find(it[6])?.value.toString())
        }
        return bookWord
    }

    // 获取小说简介
    private fun getBookSynopsis(): List<String> {
        val bookSynopsis = mutableListOf<String>()
        getBookInformation().forEach {
            bookSynopsis.add(Regex("""\s.+""").find(it[6])?.value.toString())
        }
        return bookSynopsis
    }

    private fun getBookAuthor(link: List<String>): List<String> {
        val bookAuthor = mutableListOf<String>()
        val mLink = mutableMapOf<String,String?>()
        val pool = mutableListOf<Thread>()
        for (i in link) {
            pool.add(Thread {
                val doc = Jsoup.parse(Jsoup.connect("$url$i/").get().html())
                val author = doc.selectXpath("//div[@class=\"author-name\"]/span").text()
                mLink[i] = author
            })
        }
        for (i in pool){
            i.start()
        }
        for (i in pool){
            i.join()
        }
        for (i in link){
            bookAuthor.add(mLink[i].toString())
        }
        return bookAuthor
    }
}

package com.example.sfacg_crawler.data

import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.core.content.FileProvider
import com.example.sfacg_crawler.R
import java.io.File

class OpenFiles(private val context: Context) {
    fun shareFile(record: Record) {
        try {
            val file = File(context.getExternalFilesDir(null),
                "ID-${record.id} ${record.recordName}.xlsx")
            val authority = context.packageName + ".MainActivity"
            val uri = FileProvider.getUriForFile(context, authority, file)

            context.startActivity(Intent(Intent.ACTION_SEND).apply {
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                setDataAndType(uri, "*/*")
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            })
        } catch (e: Exception) {
            Toast.makeText(context, "请先下载WPS或者MS Excel", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun readFile(record: Record) {
        try {
            val file = File(context.getExternalFilesDir(null),
                "ID-${record.id} ${record.recordName}.xlsx")
            val authority = context.packageName + ".MainActivity"
            val uri = FileProvider.getUriForFile(context, authority, file)

            context.startActivity(Intent(Intent.ACTION_VIEW).apply {
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                setDataAndType(uri, "application/vnd.ms-excel")
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            })
        } catch (e: Exception) {
            Toast.makeText(context, "请先下载WPS或者MS Excel", Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun deleteFile(
        context: Context,
        record: Record,
        position: Int,
        recordViewModel: RecordViewModel,
        shareViewModel: ShareViewModel,
    ) {
        shareViewModel.currentTaskPosition = position
        recordViewModel.whichOperation = 2
        File(context.getExternalFilesDir(null),
            "ID-${record.id} ${record.recordName}.xlsx").delete().let {
            Toast.makeText(context,
                if (it) "删除文件成功"
                else "删除文件失败", Toast.LENGTH_SHORT).show()
        }
        recordViewModel.deleteRecord(record)
    }

    fun reloadFile(
        context: Context,
        record: Record,
        position: Int,
        recordViewModel: RecordViewModel,
        shareViewModel: ShareViewModel,
    ) {
        shareViewModel.currentTaskPosition = position
        File(context.getExternalFilesDir(null),
            "ID-${record.id} ${record.recordName}.xlsx").delete().let {
            Toast.makeText(context,
                if (it) "删除原文件成功"
                else "删除原文件失败,或者原文件不存在", Toast.LENGTH_SHORT).show()
        }
        record.nowPage = record.start
        record.status = false

        recordViewModel.whichOperation = 3
        shareViewModel.taskIsInit = false
        recordViewModel.updateRecord(record)
        shareViewModel.currentTask = record

        shareViewModel.setMyToast(context.resources.getString(R.string.textview_status_start))
        shareViewModel.setDfCrawlerStatus(true)
    }
}
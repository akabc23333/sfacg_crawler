package com.example.sfacg_crawler.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Database(entities = [Record::class], version = 1, exportSchema = false)
abstract class RecordDatabase: RoomDatabase() {
    abstract fun recordDao(): RecordDao

    private class RecordDatabaseCallback(
        private val scope: CoroutineScope,
    ) : RoomDatabase.Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    populateDatabase(database.recordDao())
                }
            }
        }

        suspend fun populateDatabase(recordDao: RecordDao) {
            // Delete all content here.
            // recordDao.deleteRecord()

            // Add sample words.
            /*
            var time = LocalDateTime.now().toString().substring(0, 19).replace("T", " ").replace(":", "-")
            var record = Record(time, "test",145, 1, 0,1, false)
            recordDao.insertRecord(record)
            time = LocalDateTime.now().toString().substring(0, 19).replace("T", " ").replace(":","-")
            record = Record(time, "test", 145,1, 0, 1, false)
            recordDao.insertRecord(record)

             */

        }
    }

    companion object {

        @Volatile
        private var INSTANCE: RecordDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope,
        ): RecordDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    RecordDatabase::class.java,
                    "record_database"
                ).addCallback(RecordDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}
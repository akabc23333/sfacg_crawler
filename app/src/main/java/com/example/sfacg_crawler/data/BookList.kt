package com.example.sfacg_crawler.data

data class BookList(
    val bookName: List<String>,
    val bookCover: List<String>,
    val bookLink: List<String>,
    val bookType: List<String>,
    val bookAuthor: List<String>,
    val bookStatus: List<String>,
    val bookDate: List<String>,
    val bookWord: List<String>,
    val bookSynopsis: List<String>
)

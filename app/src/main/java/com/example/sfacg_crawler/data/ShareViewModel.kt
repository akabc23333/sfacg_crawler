package com.example.sfacg_crawler.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class ShareViewModel: ViewModel() {
    private var _myToast = MutableLiveData<String>()
    var myToast: LiveData<String> = _myToast
    fun setMyToast(text: String) = viewModelScope.launch {
        _myToast.postValue(text)
    }


    private var _nowPage = MutableLiveData<Int>()
    var nowPage: LiveData<Int> = _nowPage
    fun setNowPage(page: Int) = viewModelScope.launch {
        _nowPage.postValue(page)
    }


    private var _dfATStatus = MutableLiveData<Boolean>()
    var DfAddTaskStatus: LiveData<Boolean> = _dfATStatus.apply {
        this.postValue(false)
    }
    fun setDfAddTaskStatus(status: Boolean) = viewModelScope.launch {
        _dfATStatus.postValue(status)
    }


    private var _dfCSStatus = MutableLiveData<Boolean>()
    var DfCrawlerStatus: LiveData<Boolean> = _dfCSStatus.apply {
        this.postValue(false)
    }
    fun setDfCrawlerStatus(status: Boolean) = viewModelScope.launch {
        _dfCSStatus.postValue(status)
    }

    var setCrawlerStatusTo = false
    private var _crawlerStatus = MutableLiveData<Boolean>()
    var crawlerStatus: LiveData<Boolean> = _crawlerStatus.apply {
        this.postValue(false)
    }
    fun setCrawlerStatus(status: Boolean) = viewModelScope.launch {
        _crawlerStatus.postValue(status)
    }

    lateinit var currentTask: Record

    var taskIsInit = false

    var currentTaskPosition = 0

}
package com.example.sfacg_crawler.excel

import android.content.Context
import com.example.sfacg_crawler.data.BookList
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOError

class ExcelFile(con: Context) {
    private val context = con
    private var dirPath = context.getExternalFilesDir(null)
    private lateinit var workBook: XSSFWorkbook
    private lateinit var workSheet: XSSFSheet
    var sum = 0
    var start = 1
    var end = 0

    fun initExcelFile(fileName: String, stag: Int, start: Int, end: Int): Boolean {
        val file = File(context.getExternalFilesDir(null), fileName).isFile
        if (file) {
            val inputStream = FileInputStream("$dirPath/$fileName")
            workBook = XSSFWorkbook(inputStream)
            workSheet = workBook.getSheet("base_information")
            var base = workSheet.getRow(1)
            sum = base.getCell(1).rawValue.toDouble().toInt()
            base = workSheet.getRow(2)
            this.start = base.getCell(1).rawValue.toDouble().toInt()
            base = workSheet.getRow(3)
            this.end = base.getCell(1).rawValue.toDouble().toInt()
            inputStream.close()
            return true
        }
        val result: Boolean = File(context.getExternalFilesDir(null), fileName).createNewFile()
        if (result) {
            workBook = XSSFWorkbook()
            workSheet = workBook.createSheet("base_information")

            var biRow = workSheet.createRow(0)
            biRow.createCell(0).setCellValue("标签代码")
            biRow.createCell(1).setCellValue(stag.toDouble())
            biRow = workSheet.createRow(1)
            biRow.createCell(0).setCellValue("总数")
            biRow.createCell(1).setCellValue(sum.toDouble())
            biRow = workSheet.createRow(2)
            biRow.createCell(0).setCellValue("开始页面")
            biRow.createCell(1).setCellValue(start.toDouble())
            biRow = workSheet.createRow(3)
            biRow.createCell(0).setCellValue("结束页面")
            biRow.createCell(1).setCellValue(end.toDouble())

            val tableTitle = mapOf(0 to "书名", 1 to "作者", 2 to "类型", 3 to "更新状态",
                4 to "最后更新日期", 5 to "字数", 6 to "简介", 7 to "小说链接", 8 to "封面链接")
            val xlRow = workSheet.createRow(4)
            for (i in tableTitle) {
                xlRow.createCell(i.key).setCellValue(i.value)
            }

            val outputStream = FileOutputStream("$dirPath/$fileName")
            workBook.write(outputStream)
            outputStream.close()
        }
        // Looper.prepare()
        // Toast.makeText(context, "file is $file so add file", Toast.LENGTH_SHORT).show()
        // Looper.loop()
        return false
    }

    fun writeExcelFile(fileName: String, data: BookList): Boolean {
        val size = data.bookName.size
        var a = 0
        while (a < size) {
            val row = workSheet.createRow(sum + 5)
            row.createCell(0).setCellValue(data.bookName[a])
            row.createCell(1).setCellValue(data.bookAuthor[a])
            row.createCell(2).setCellValue(data.bookType[a])
            row.createCell(3).setCellValue(data.bookStatus[a])
            row.createCell(4).setCellValue(data.bookDate[a])
            row.createCell(5).setCellValue(data.bookWord[a])
            row.createCell(6).setCellValue(data.bookSynopsis[a])
            row.createCell(7).setCellValue(data.bookLink[a])
            row.createCell(8).setCellValue(data.bookCover[a])
            sum++
            workSheet.getRow(1).getCell(1).setCellValue(sum.toDouble())
            a++
        }
        val outputStream = FileOutputStream("$dirPath/$fileName")
        try {
            workBook.write(outputStream)
        } catch (e: IOError) {
            return false
        }
        outputStream.close()
        return true
    }
    fun allClose(){
        workBook.close()
    }
}
package com.example.sfacg_crawler

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowInsetsController
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.sfacg_crawler.data.*
import com.example.sfacg_crawler.databinding.ActivityMainBinding
import com.example.sfacg_crawler.ui.DialogFragmentAddTask
import com.example.sfacg_crawler.ui.DialogFragmentCrawler
import com.example.sfacg_crawler.ui.RecordListAdapter


class MainActivity : AppCompatActivity() {
    private val recordViewModel: RecordViewModel by viewModels {
        RecordViewModelFactory((application as RecordApplication).repository)
    }
    private lateinit var shareViewModel: ShareViewModel
    private val adapter = RecordListAdapter()
    private val DfAddTask = DialogFragmentAddTask()
    private val DfCrawlerStatus = DialogFragmentCrawler()
    private val openFiles = OpenFiles(this)

    @SuppressLint("NotifyDataSetChanged")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        shareViewModel = ViewModelProvider(this)[ShareViewModel::class.java]

        window.statusBarColor = ContextCompat.getColor(this, R.color.bili_pink)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.decorView.windowInsetsController?.setSystemBarsAppearance(
                WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS,
                WindowInsetsController.APPEARANCE_LIGHT_STATUS_BARS)
        } else {
            @Suppress("DEPRECATION")
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
        }

        binding.recyclerview.adapter = adapter


        recordViewModel.allRecord.observe(this) {
            adapter.recordList = it.reversed().toMutableList()
            when (recordViewModel.whichOperation) {
                1 -> {
                    adapter.notifyItemInserted(0)
                    binding.recyclerview.layoutManager?.scrollToPosition(0)
                    adapter.notifyItemRangeChanged(1,
                        it.size - shareViewModel.currentTaskPosition)
                    shareViewModel.currentTask = it.last()
                    shareViewModel.currentTaskPosition = 0
                    shareViewModel.taskIsInit = true
                }
                2 -> {
                    adapter.notifyItemRemoved(shareViewModel.currentTaskPosition)
                    adapter.notifyItemRangeChanged(shareViewModel.currentTaskPosition,
                        it.size - shareViewModel.currentTaskPosition)
                }
                3 -> {
                    Log.d("a", recordViewModel.whichOperation.toString())
                    adapter.notifyItemChanged(shareViewModel.currentTaskPosition)
                    shareViewModel.taskIsInit = true
                }
                else -> adapter.notifyDataSetChanged()

            }
            Log.d("a", "all record")
        }
        adapter.onItemCardViewClick = { position, record, button ->
            when (button) {
                0 -> {
                    openFiles.readFile(record)
                }
                1 -> {
                    openFiles.shareFile(record)
                }
                2 -> {
                    openFiles.deleteFile(this, record, position, recordViewModel, shareViewModel)
                }

                3 -> {
                    openFiles.reloadFile(this, record, position, recordViewModel, shareViewModel)
                }
            }
        }

        shareViewModel.DfAddTaskStatus.observe(this) {
            if (it) {
                if (DfAddTask.isAdded) DfAddTask.dialog?.show()
                else DfAddTask.show(supportFragmentManager, "dfAT")
            } else {
                DfAddTask.dialog?.dismiss()
            }
        }

        shareViewModel.DfCrawlerStatus.observe(this) {
            Log.d("MainActivity", "")
            if (it) {
                DfCrawlerStatus.show(supportFragmentManager, "dfCS")
            }
        }

        shareViewModel.crawlerStatus.observe(this) {
            Log.d("MainActivity", "crawler status change: $it")
            binding.imageViewCrawlerStatus.setImageResource(
                if (it) {
                    R.color.crawler_start
                } else {
                    R.color.crawler_stop
                }
            )
        }


        binding.buttonAdd.setOnClickListener {

            Log.d("MainActivity", "click add button")
            shareViewModel.crawlerStatus.value?.let {
                if (it)
                    shareViewModel.setDfCrawlerStatus(true)
                else
                    shareViewModel.setDfAddTaskStatus(true)
            }
        }
    }

}
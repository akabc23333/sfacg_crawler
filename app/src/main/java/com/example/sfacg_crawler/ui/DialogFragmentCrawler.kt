package com.example.sfacg_crawler.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import com.example.sfacg_crawler.R
import com.example.sfacg_crawler.data.Crawler
import com.example.sfacg_crawler.data.Record
import com.example.sfacg_crawler.data.RecordViewModel
import com.example.sfacg_crawler.data.ShareViewModel
import com.example.sfacg_crawler.databinding.DialogfragmentCrawlerStatusBinding
import com.example.sfacg_crawler.excel.ExcelFile


class DialogFragmentCrawler : BaseDialogFragment<DialogfragmentCrawlerStatusBinding>() {
    private val shareViewModel: ShareViewModel by activityViewModels()
    private val recordViewModel: RecordViewModel by activityViewModels()
    private lateinit var binding: DialogfragmentCrawlerStatusBinding
    private lateinit var excelFile: ExcelFile
    private lateinit var crawler: Crawler

    override val bindingInflater: (LayoutInflater, ViewGroup?, Bundle?) -> DialogfragmentCrawlerStatusBinding
        get() = { layoutInflater, viewGroup, _ ->
            binding = DialogfragmentCrawlerStatusBinding.inflate(layoutInflater, viewGroup, false)
            binding
        }

    override fun onStart() {
        shareViewModel.myToast.observe(this) {
            binding.textViewDialog.text = it
        }

        shareViewModel.nowPage.observe(this) {
            binding.textViewPace.text = getString(R.string.textview_pace,
                it.toString(),
                shareViewModel.currentTask.end.toString())
        }
        shareViewModel.crawlerStatus.observe(this) {
            binding.buttonCrawlerContrl.text =
                if (it) "暂停"
                else "开始"
        }
        binding.buttonCrawlerContrl.setOnClickListener {
            if (shareViewModel.taskIsInit) {
                shareViewModel.crawlerStatus.value?.let { status ->
                    if (!status) {
                        Log.d("DFCS", "create thread")
                        Thread {
                            shareViewModel.setCrawlerStatusTo = true
                            shareViewModel.setCrawlerStatus(true)
                            val result = try {
                                startCrawler(shareViewModel.currentTask)
                            }catch (e: Exception){
                                Log.d("DfCrawler", "$e")
                                false
                            }
                            shareViewModel.setMyToast(
                                if (result) {
                                    "已完成任务"
                                } else {
                                    "已暂停"
                                })
                            shareViewModel.setCrawlerStatus(false)
                        }.start()
                    } else {
                        shareViewModel.setCrawlerStatusTo = false
                    }
                }
            } else {
                Toast.makeText(activity, "当前任务还未记录入数据库，请稍后重试", Toast.LENGTH_SHORT).show()
            }
        }
        super.onStart()
    }

    private fun startCrawler(task: Record): Boolean {
        excelFile = ExcelFile(requireActivity())
        crawler = Crawler()
        shareViewModel.setMyToast("初始化中...")
        val fileName = "ID-${task.id} ${task.recordName}.xlsx"
        Log.d("DFCS", excelFile.initExcelFile(fileName, task.stag, task.start, task.end).toString())
        while (task.nowPage <= task.end || task.end == 0) {
            if (!shareViewModel.setCrawlerStatusTo) {
                return false
            }
            shareViewModel.setMyToast("爬取第${task.nowPage}页...")
            shareViewModel.setNowPage(task.nowPage)
            val result = try {
                crawler.request(task.stag, task.nowPage)
            } catch (e: Exception) {
                shareViewModel.setMyToast(e.toString())
                false
            }
            if (result) {
                if (crawler.result.bookName.isEmpty()) {
                    break
                }else{
                    shareViewModel.setMyToast("爬取第${task.nowPage}页成功")
                    shareViewModel.setMyToast("写入第${task.nowPage}页...")
                    if (!excelFile.writeExcelFile(fileName, crawler.result)) {
                        Log.e("DfCrawler", "input page ${task.nowPage} excel error")
                        shareViewModel.setMyToast("写入excel文件第 ${task.nowPage} 页错误")
                    }
                    shareViewModel.setMyToast("第${task.nowPage}页写入excel文件成功")
                }
            } else {
                Log.e("DfCrawler", "crawler page ${task.nowPage} network error")
                shareViewModel.setMyToast("爬虫第 ${task.nowPage} 页网络错误")
            }
            task.nowPage++
            recordViewModel.whichOperation = 3
            recordViewModel.updateRecord(task)
        }
        task.status = true
        recordViewModel.updateRecord(task)
        excelFile.allClose()
        shareViewModel.setMyToast("全部爬取完毕")
        return true
    }

    override fun onHiddenChanged(hidden: Boolean) {
        super.onHiddenChanged(hidden)
    }
}
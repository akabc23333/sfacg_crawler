package com.example.sfacg_crawler.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.sfacg_crawler.R
import com.example.sfacg_crawler.data.Record
import com.example.sfacg_crawler.databinding.ItemBookListBinding


class RecordListAdapter : RecyclerView.Adapter<RecordListAdapter.MyViewHolder>() {

    var recordList = mutableListOf<Record>()
    lateinit var onItemCardViewClick: (position: Int, record: Record, button: Int) -> Unit

    class MyViewHolder(var itemBinding: ItemBookListBinding) : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(
            position: Int,
            time: String,
            stag:Int,
            start: Int,
            end: Int,
            name: String,
            status: Boolean,
        ) {
            val pos = position + 1
            itemBinding.textViewPosition.text = pos.toString()
            itemBinding.textViewTime.text = time
            itemBinding.textViewStag.text = stag.toString()
            val order = "$start To $end"
            itemBinding.textViewPage.text = order
            itemBinding.textViewName.text = name
            if (status)
                itemBinding.imageViewStatus.setBackgroundResource(R.color.task_unfinished)
            else
                itemBinding.imageViewStatus.setBackgroundResource(R.color.task_finished)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemBinding = ItemBookListBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(
            position,
            recordList[position].time,
            recordList[position].stag,
            recordList[position].start,
            recordList[position].end,
            recordList[position].recordName,
            recordList[position].status
        )
        holder.itemBinding.buttonShare.setOnClickListener {
            onItemCardViewClick.invoke(position, recordList[position], 1)
        }
        holder.itemBinding.buttonDelete.setOnClickListener {
            onItemCardViewClick.invoke(position, recordList[position], 2)
        }
        holder.itemBinding.buttonRepost.setOnClickListener {
            onItemCardViewClick.invoke(position, recordList[position], 3)
        }
        holder.itemView.setOnClickListener {
            onItemCardViewClick.invoke(position, recordList[position], 0)
        }
    }

    override fun getItemCount() = recordList.size
}




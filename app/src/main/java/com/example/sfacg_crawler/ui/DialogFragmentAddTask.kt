package com.example.sfacg_crawler.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.sfacg_crawler.data.Record
import com.example.sfacg_crawler.data.RecordViewModel
import com.example.sfacg_crawler.data.ShareViewModel
import com.example.sfacg_crawler.databinding.DialogfragmentAddTaskBinding
import java.time.LocalDateTime

class DialogFragmentAddTask:BaseDialogFragment<DialogfragmentAddTaskBinding>() {
    private lateinit var binding: DialogfragmentAddTaskBinding
    private val shareViewModel: ShareViewModel by activityViewModels()
    private val recordViewModel: RecordViewModel by activityViewModels()

    override val bindingInflater: (LayoutInflater, ViewGroup?, Bundle?) -> DialogfragmentAddTaskBinding
        get() = { layoutInflater, viewGroup, _ ->
            binding = DialogfragmentAddTaskBinding.inflate(layoutInflater, viewGroup, false)
            binding
        }

    override fun onStart() {
        binding.buttonCrawlerStart.setOnClickListener {
            shareViewModel.setDfAddTaskStatus(false)

            val time = LocalDateTime.now().toString().substring(0, 19)
                .replace("T", " ").replace(":", "-")
            val record = Record(
                time,
                binding.edittextTaskname.text.toString(),
                binding.edittextNumberTag.text.toString().toInt(),
                binding.edittextNumberStart.text.toString().toInt(),
                binding.edittextNumberEnd.text.toString().toInt(),
                binding.edittextNumberStart.text.toString().toInt(), false)
            recordViewModel.whichOperation = 1
            shareViewModel.taskIsInit = false
            recordViewModel.insertRecord(record)

            shareViewModel.setDfCrawlerStatus(true)
        }
        super.onStart()
    }


}